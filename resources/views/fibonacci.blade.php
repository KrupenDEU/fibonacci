@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Fibonacci Calculator</h1>
        <div class="form-group">
            <label for="from">From:</label>
            <input type="number" id="from" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="to">To:</label>
            <input type="number" id="to" class="form-control" required>
        </div>
        <button id="calculate" class="btn btn-primary">Calculate</button>

        <h2>Result</h2>
        <p id="result"></p>
    </div>
@endsection

@section('scripts')
    <script>
        document.getElementById('calculate').addEventListener('click', function() {
            const from = document.getElementById('from').value;
            const to = document.getElementById('to').value;
            const resultElement = document.getElementById('result');

            fetch(`/api/fibonacci?from=${from}&to=${to}`)
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Network response was not ok');
                    }
                    return response.json();
                })
                .then(data => {
                    resultElement.textContent = data.data;
                })
                .catch(error => {
                    console.error('There has been a problem with your fetch operation:', error);
                    resultElement.textContent = 'Error fetching the result';
                });
        });
    </script>
@endsection

