<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::table('fibonacci', function (Blueprint $table) {
            $table->integer('from_value');
            $table->integer('to_value');
            $table->string('sequence');
        });
    }

    public function down(): void
    {
        Schema::table('fibonacci', function (Blueprint $table) {
            $table->dropColumn(['from', 'to', 'sequence']);
            $table->dropColumn('sequence');
            $table->dropTimestamps();
        });
    }
};
