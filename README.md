# fibonacci
    
## Описание

Тестовое задание

## Старт

Команда для создания и запуска контейнеров (удаляет и создает новые volumes)
```
docker-compose up -d --build 
```
Команда для запуска контейнеров
```
docker-compose up -d
```
Команда для стопа контейнеров
```
docker-compose down
```

## Полезные ресурсы

- [ ] [Laravel](https://laravel.com/docs/9.x) - официальная документация
- [ ] [Postgres](https://www.postgresql.org/docs/current/index.html) - официальная документация
- [ ] [Redis](https://redis.io/docs/) - официальная документация
- [ ] [Docker](https://docs.docker.com) - официальная документация
