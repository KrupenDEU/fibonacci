<?php

/**
 * @noinspection PhpUnnecessaryFullyQualifiedNameInspection
 */

declare(strict_types=1);

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Fibonacci
 *
 * @property int $id
 * @property int $from_value
 * @property int $to_value
 * @property string $sequence
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @mixin Eloquent
 */
class Fibonacci extends Model
{
    protected $table = 'fibonacci';

    protected $fillable = [
        'from_value',
        'to_value',
        'sequence',
    ];
}
