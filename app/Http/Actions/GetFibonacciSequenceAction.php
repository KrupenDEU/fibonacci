<?php

declare(strict_types=1);

namespace App\Http\Actions;

use App\Models\Fibonacci;
use RedisException;
use TheSeer\Tokenizer\Exception;
use Redis;

final class GetFibonacciSequenceAction
{
    /**
     * @throws RedisException
     */
    public function __construct(
        public Redis $redis,
    )
    {
        $this->redis->connect('redis');
    }

    /**
     * @throws Exception
     * @throws RedisException
     */
    public function run(int $from, int $to): string
    {
        $key = $this->buildKey($from, $to);
        if (!$this->redis->exists($key) && !$this->processing($key, $from, $to)) {
            throw new Exception('Failed to calculate');
        }

        return $this->redis->get($key);
    }

    private function buildKey(int $from, int $to): string
    {
        return "from:$from:to:$to";
    }

    /**
     * @throws RedisException
     */
    private function processing(string $key, int $from, int $to): bool
    {
        $sequence = array_map(function ($q) {
            return $this->calculateFib($q);
        }, range($from, $to));

        $value = implode(',', $sequence);
        $this->storeInDB($from, $to, $value);

        return $this->redis->set($key, $value);
    }

    private function calculateFib(int $n): int
    {
        if ($n <= 1) {
            return $n;
        } else {
            return $this->calculateFib($n - 1) + $this->calculateFib($n - 2);
        }
    }

    private function storeInDB(int $from, int $to, string $sequence): void
    {
        $fibonacci = new Fibonacci();
        $fibonacci->to_value = $to;
        $fibonacci->from_value = $from;
        $fibonacci->sequence = $sequence;
        $fibonacci->save();
    }
}
