<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

final class CalculateFibonacciRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'to' => 'required|int',
            'from' => 'required|int|min:1|lt:to',
        ];
    }
}
