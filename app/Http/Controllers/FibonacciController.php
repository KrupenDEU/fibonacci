<?php

namespace App\Http\Controllers;

use App\Http\Actions\GetFibonacciSequenceAction;
use App\Http\Requests\CalculateFibonacciRequest;
use Illuminate\Http\JsonResponse;
use TheSeer\Tokenizer\Exception;
use RedisException;

class FibonacciController extends Controller
{
    /**
     * @throws RedisException
     */
    public function getFibonacciRange(CalculateFibonacciRequest $request, GetFibonacciSequenceAction $action): JsonResponse
    {
        $from = $request->get('from');
        $to = $request->get('to');

        try {
            $result = $action->run($from, $to);
            return response()->json(['data' => $result]);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

}
